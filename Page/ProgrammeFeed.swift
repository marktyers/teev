//
//  ProgrammeFeed.swift
//  Page
//
//  Created by Mark Tyers on 26/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

// 86400 seconds in 24 hours


import Foundation
import UIKit

struct ScheduleInfo {
    var channelTitle:String
    var title:String
    var description:String
    var start:NSDate
    var duration:Int
}

struct Branding {
    var title:String
    var image:UIImage
}

class ProgrammeFeed {
    class func getSchedule(channel:String, completion: (result:Array<ScheduleInfo>)->()) {
        let today = NSDate()
        let timestamp:Int = Int(today.timeIntervalSince1970)
        let end = timestamp+14400
        let url = NSURL(string: "https://atlas.metabroadcast.com/3.0/schedule.json?channel_id=\(channel)&publisher=bbc.co.uk&annotations=channel,description,broadcasts&from=\(timestamp)&to=\(end)")
        //println(url)
        if let tempUrl:NSURL = url {
            //println(1)
            let urlReq = NSURLRequest(URL: tempUrl)
            let queue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if (error != nil) {
                    //println(2)
                    println("API error: \(error), \(error.userInfo)")
                }
                //println(3)
                var jsonError:NSError?
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                if (jsonError != nil) {
                    //println(4)
                    println("Error parsing json: \(jsonError)")
                }
                else {
                    //println(5)
                    if let schedules = json["schedule"] as? NSArray {
                        var scheduleArray = [ScheduleInfo]()
                        for schedule in schedules {
                            var scheduleItem = ScheduleInfo(channelTitle: "", title: "", description: "", start: NSDate(), duration: 0)
                            if let channel_title = schedule["channel_title"] as? String {
                                scheduleItem.channelTitle = channel_title
                            }
                            if let items = schedule["items"] as? NSArray {
                                for item in items {
                                    if let title = item["title"] as? String {
                                        scheduleItem.title = title
                                    }
                                    if let description = item["description"] as? String {
                                        scheduleItem.description = description
                                    }
                                    if let broadcasts = item["broadcasts"] as? NSArray {
                                        for broadcast in broadcasts {
                                            if let start = broadcast["transmission_time"] as? String {
                                                var formatter = NSDateFormatter()
                                                formatter.dateFormat = "YYYY-MM-DD'T'HH:mm:ss'Z'"
                                                if let date = formatter.dateFromString(start) {
                                                    scheduleItem.start = date
                                                }
                                            }
                                            if let duration = broadcast["duration"] as? Int {
                                                scheduleItem.duration = duration/60
                                            }
                                        }
                                    }
                                    scheduleArray.append(scheduleItem)
                                }
                            }
                        }
                        completion(result: scheduleArray)
                    }
                }
            })
        }
    }
    
    class func getBranding(channel:String, completion: (result:Branding)->()) {
        let today = NSDate()
        let timestamp:Int = Int(today.timeIntervalSince1970)
        let url = NSURL(string: "https://atlas.metabroadcast.com/3.0/schedule.json?channel_id=\(channel)&publisher=bbc.co.uk&annotations=channel&from=\(timestamp)&to=\(timestamp)")
        //println(url)
        if let tempUrl:NSURL = url {
            //println(1)
            let urlReq = NSURLRequest(URL: tempUrl)
            let queue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if (error != nil) {
                    //println(2)
                    println("API error: \(error), \(error.userInfo)")
                }
                //println(3)
                var jsonError:NSError?
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                if (jsonError != nil) {
                    //println(4)
                    println("Error parsing json: \(jsonError)")
                }
                else {
                    //println(5)
                    if let schedules = json["schedule"] as? NSArray {
                        var scheduleArray = [ScheduleInfo]()
                        for schedule in schedules {
                            var branding = Branding(title: "",image: UIImage())
                            if let channel_title = schedule["channel_title"] as? String {
                                branding.title = channel_title
                            }
                            if let channelInfo = schedule["channel"] as? NSDictionary {
                                if let images = channelInfo["images"] as? NSArray {
                                    for image in images {
                                        if image["theme"] as? String == "light_transparent" {
                                            var logo = image["uri"] as String
                                            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
                                            println(paths)
                                            println("channel \(channel)")
                                            //let path = paths+"xxx.png"
                                            //println("path \(path)")
                                            var getImagePath = paths.stringByAppendingPathComponent("\(channel).png")
                                            //let getImagePath = "\(paths)\(channel).png"
                                            println("imagePath: \(getImagePath)")
                                            var checkValidation = NSFileManager.defaultManager()
                                            if (checkValidation.fileExistsAtPath(getImagePath)) {
                                                println("local image found")
                                                println("\(channel).png")
                                                branding.image = UIImage(contentsOfFile: getImagePath)!
                                            } else {
                                                println()
                                                println("download file");
                                                println("logo url: \(logo)")
                                                if let url = NSURL(string: logo) {
                                                    //println("url: \(url)")
                                                    if let data = NSData(contentsOfURL: url) {
                                                        if let image:UIImage = UIImage(data: data) {
                                                            UIImagePNGRepresentation(image).writeToFile(getImagePath, atomically: true)
                                                            branding.image = image
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            completion(result: branding)
                        }
                    }
                }
            })
        }
    }
}
