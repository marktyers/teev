
import UIKit
import Social

class ViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {
    
    var channels:[String] = ["cbbh", "cbbG", "cbbP", "cbbQ"]
    var programmeArray = [ScheduleInfo]()

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet var right: UISwipeGestureRecognizer!
    @IBOutlet var left: UISwipeGestureRecognizer!
    @IBOutlet weak var programmes: UITableView!
    @IBOutlet var tapScreen: UITapGestureRecognizer!
    //@IBOutlet weak var bottomBar: UIToolbar!
    @IBOutlet weak var toolbarBase: NSLayoutConstraint!
    @IBOutlet weak var pageIndicatorBase: NSLayoutConstraint!

    @IBAction func launchActivity(sender: UIBarButtonItem) {
        var message = [AnyObject]()
        if let text = self.pageNumber.text {
            message.append("Watching \(text)")
        }
        if let image:UIImage = self.logo.image {
            message.append(image)
        }
        let activity:UIActivityViewController = UIActivityViewController(activityItems: message, applicationActivities: nil)
        self.presentViewController(activity, animated: true, completion: nil)
    }
    
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        if self.toolbarBase.constant == 0 {
            UIView.animateWithDuration(0.5, animations: {
                self.toolbarBase.constant = -44
                self.pageIndicatorBase.constant -= 44
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animateWithDuration(0.5, animations: {
                self.toolbarBase.constant = 0
                self.pageIndicatorBase.constant += 44
                self.view.layoutIfNeeded()
            })
            
        }
    }

    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        println("swipe left")
        UIView.animateWithDuration(1.0, animations: {
            self.programmes.alpha = 0.0
            self.logo.alpha = 0.0
            self.pageNumber.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.pageIndicator.currentPage++
                self.getSchedule()
                self.getBranding()
                UIView.animateWithDuration(1.0, delay: 0.5, options: nil, animations: {
                    self.programmes.alpha = 1.0
                    self.logo.alpha = 1.0
                    self.pageNumber.alpha = 1.0
                }, completion: nil)
        })
    }
    
    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        println("swipe right")
        UIView.animateWithDuration(1.0, animations: {
            self.programmes.alpha = 0.0
            self.logo.alpha = 0.0
            self.pageNumber.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.pageIndicator.currentPage--
                self.getSchedule()
                self.getBranding()
                UIView.animateWithDuration(1.0, delay: 0.5, options: nil, animations: {
                    self.programmes.alpha = 1.0
                    self.logo.alpha = 1.0
                    self.pageNumber.alpha = 1.0
                    }, completion: nil)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.programmes.dataSource = self
        self.programmes.delegate = self
        self.view.addGestureRecognizer(self.right)
        self.view.addGestureRecognizer(self.left)
        self.view.addGestureRecognizer(self.tapScreen)
        self.pageNumber.text = ""
        self.pageIndicator.numberOfPages = channels.count
        self.getSchedule()
        self.getBranding()
        UIView.animateWithDuration(2.5, animations: {
            self.toolbarBase.constant = -44
            self.view.layoutIfNeeded()
        })
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programmeArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProgrammeCell", forIndexPath: indexPath) as UITableViewCell
        let data = self.programmeArray[indexPath.row] as ScheduleInfo
        cell.textLabel.text = data.title
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBranding() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        self.pageNumber.text = ""
        let channel = self.channels[self.pageIndicator.currentPage]
        ProgrammeFeed.getBranding(channel, completion: {(result:Branding) in
            dispatch_async(dispatch_get_main_queue(), {
                self.logo.image = result.image
                self.pageNumber.text = result.title
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            })

        })
    }

    func getSchedule() {
        let channel = self.channels[self.pageIndicator.currentPage]
        ProgrammeFeed.getSchedule(channel, completion: {(result: Array<ScheduleInfo>) in
            dispatch_async(dispatch_get_main_queue(), {
                self.programmeArray = result
                self.programmes.reloadData()
                self.programmes.setContentOffset(CGPointZero, animated: true)
            })
            for item in result {
                //println("channel title: \(item.channelTitle)")
                //println("programme title: \(item.title)")
                //println("programme description: \(item.description)")
                //println("programme start: \(item.start)")
                //println("duration: \(item.duration)")
            }
            
        })
    }

}

